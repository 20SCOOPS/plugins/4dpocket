#!/bin/sh

BIN_PATH=/usr/local/bin/4dpocket
INSTALLER_PATH=/usr/local/bin/4dpocket_installer
PACKAGE_ROOT=/usr/local/share
PACKAGE_PATH=$PACKAGE_ROOT/4dpocket
GIT_SRC=https://gitlab.com/20SCOOPS/plugins/4dpocket.git
README=https://gitlab.com/20SCOOPS/plugins/4dpocket/blob/master/README.md
SCRIPTS_DIR=4dpocket-scripts

skip=-1
install=-1
i=1

fdp_download()
{
    cd $PACKAGE_ROOT
    git clone $GIT_SRC
} 
fdp_check_update()
{
    cd $PACKAGE_PATH

    git fetch

    UPSTREAM='@{u}'
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    git reset --hard >/dev/null
    if [ $LOCAL = $REMOTE ]; then
# no update
        return 0
    elif [ $LOCAL = $BASE ]; then
# have update
        return 1
    else
# error
        return -1
    fi
}
fdp_update()
{
    fdp_check_update $1
    updateStatus=$?
    cd $PACKAGE_PATH

    echo "checking update $updateStatus"

    git reset --hard >/dev/null
    if [ $updateStatus = 0 ]; then
        echo "up-to-date"
        return 0 #no update skip install
    elif [ $updateStatus = 1 ]; then
        echo "updating new version"
        git pull
        return 1 #install
    else
        echo "something wrong. (may be someone did something with file in /usr/local/share/4dpocket)"
        return -1 #error
    fi
} 
fdp_install() 
{
    cd $PACKAGE_PATH    
# move file binary
    cp tools $BIN_PATH
    chmod 777 $BIN_PATH
# move file shell installer
    cp installer.sh $INSTALLER_PATH
    chmod 777 $INSTALLER_PATH

    echo "install '4dpocket' version $(4dpocket version) completed"
}
fdp_uninstall() 
{
    if [ $1 -ne 0 ]
    then 
        echo "uninstall '4dpocket'? (y/n or any key):"
        read -n 1 accept; echo ""
    else accept=y; fi
    if [ "$accept" = "y" ]
    then
        rm -f $BIN_PATH
        rm -rf $PACKAGE_PATH
        echo "uninstall '4dpocket' completed"
    else
        echo "cancel"
    fi
}
fdp_remove_installer()
{
if [ $1 -ne 0 ]
    then 
        echo "remove '4dpocket' installer? (y/n or any key):"
        read -n 1 accept; echo ""
    else accept=y; fi
    if [ "$accept" = "y" ]
    then
        rm -f $INSTALLER_PATH
        echo "remove '4dpocket' installer completed"
    else
        echo "cancel"
    fi
}
fdp_checkIfAvaliable(){
    if $1 2>/dev/null; then return 0; else return -1; fi
}

fdp_copy_scripts(){
    path=${1:-"."}
    if [ ! -d "$path" ]; then echo "no such directory ( $path )"; exit -1; fi
    path=$path/$SCRIPTS_DIR
    if [ ! -d "$path" ]; then mkdir $path; fi
    echo "copy script to $path"
    # copy shellscripts & chmod
    cp $PACKAGE_PATH/hyky.sh $path/hyky.sh; chmod 777 $path/hyky.sh
    echo "done"
}
fdp_readme()
{
    echo "open readme '$README'"
    open $README
}
clear
if [ ${!i:-0} = "readme" ]
then 
    fdp_readme
    exit 0
fi
if [ ${!i:-0} = "skip" ]
then 
    skip=0
    i=$((i+1))
fi
if [ ${!i:-0} = "get" ]
then 
    i=$((i+1))
    fdp_copy_scripts ${!i}
    exit 0
elif [ ${!i:-0} = "uninstall" ]
then 
    fdp_uninstall $skip
    fdp_remove_installer $skip
    exit 0
fi

# check if git is installed
echo "preparing..."
fdp_checkIfAvaliable "git --version" >/dev/null
if ! [ $? -eq 0 ]; then echo "please install git"; exit -1; fi

if [ ${!i:-0} = "force" ]
then 
    fdp_uninstall $skip
    fdp_download $skip
    fdp_install $skip
    exit 0
elif [ ${!i:-0} = "check" ]
then 
    fdp_check_update $skip
    echo $?
    exit $?
elif [ ${!i:-"install"} = "install" ]
then
    # if tools is installed
    fdp_checkIfAvaliable "4dpocket version" >/dev/null
    if ! [ $? -eq 0 ]
    then
        fdp_download $skip;
        install=1
    else
        echo "u have '4dpocket' version $(4dpocket version)"
        fdp_update $skip
        install=$?
    fi
    if [ $install -eq 1 ]; then fdp_install $skip; fi

    exit $install
fi

echo "unavaliable command"
fdp_readme
exit -1