# 4DPOCKET 🔵🤖😺#กระเป๋า4มิติ

[Download Installer Here!<br>![Download installer](https://goo.gl/pJaE9c)](https://s3.eu-central-1.amazonaws.com/20scoops-app/4dpocket/4dpocket_installer.sh)

[Slack chat](https://20scoopscnx.slack.com/messages/C7YJQ8HE3)

## requirement

- **for macos only**
- macos 10.13+
- lower than that i don't know

## installation
- download installer file link above or use command to download

        curl https://s3.eu-central-1.amazonaws.com/20scoops-app/4dpocket/4dpocket_installer.sh>4dpocket_installer.sh

- run downloaded installer file

        cd $DOWNLOAD_DIRECTORY
        chmod 777 4dpocket_installer.sh
        ./4dpocket_installer.sh

- delete downloaded installer file *( cause u can use installer anywhere now )*

        rm 4dpocket_installer.sh

## installer command


- install/update tools *( after installed if 4dpocket_installer not remove yet )*

        4dpocket_installer
        or
        4dpocket_installer install

- check update will echo and return result => '-1:error' 0:'no update' 1:'have update'

        4dpocket_installer check
        
- uninstall tools 

        4dpocket_installer uninstall

- copy scripts to project directory *( this will create '4dpocket-scripts' directory at project directory )*

        4dpocket_installer get $project_directory

- skip asking

        4dpocket_installer skip $command

- open brower see to this readme *( and any wrong command )*

        4dpocket_installer readme

## tools command

- check version

        4dpocket version

- honyak 🗣

        4dpocket honyak $platform $json_source  $destination_path $config

- konyak 🍮

        4dpocket konyak $src_type $csv_source $json_destination

## avaliable script ( 1 )

### hyky 🗣🍮 #วุ้นแปลภาษา

-  run

        cd $project_directory
        ./4dpocket-scripts/hyky.sh $platform $src_url $destination_path

- supported platforms

  - ios (.strings)
  - android (.xml)
  - php (.php)
  - js (.js) **u know js is javascript*
  - sketch (.json)
