#!/bin/sh

echo start

# hyky - generate platform's langauge files  from csv
# ./4dpocket-scripts/hyky.sh $platform $src_url $destination_path

# All arguments required!
# platform => ios, android, php, js or sketch
# src_url => googlesheet link(with gid), csv url or .csv file path
# destination_path => project directory or ur destination

# platform -> ios,android,php,js,sketch
platform=$1

# source type -> csv (just csv for now)
src_type=csv

# source file 
src=$2

# destination path
# des=$PRODUCT_NAME
des=$3

# config of each platform
config=/usr/local/share/4dpocket/config.json

# temporary json file from src
file_temp=~/hyky.temp.json

# run tools
if ! [ -f "/usr/local/bin/4dpocket" ] 
then 
    echo "No 4dpocket tools be found"
    exit 0
fi
#scripts
4dpocket konyak $src_type "$src" "$file_temp"
4dpocket honyak $platform "$file_temp" "$des" "$config"
rm "$file_temp"

exit 0